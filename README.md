ctrlp-py-matcher
================

A fork of the ctrlp-py-matcher plugin that changes the search criteria to "contains-all" and runs faster on very large projects.
